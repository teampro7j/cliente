/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author juan
 */
public class Cliente {
    private ObjectInputStream input;
    private ObjectOutputStream output;
    private Socket socket;
    
    private String server;
    private int port;
    
    public Cliente(String server, int port){
        this.server = server;
        this.port = port;
    }
    public void start() {
        try {
            this.socket = new Socket(this.server, this.port);
            this.output = new ObjectOutputStream(this.socket.getOutputStream());
            this.input = new ObjectInputStream(this.socket.getInputStream());
        } catch(Exception e) {
            System.out.println("Error al conectarse al servidor " + e);
        }
    }
    public void sendMessage(Resources.Message message) {
       try {
            this.output.writeObject(message);
            System.out.println("Mensaje enviado");
        } catch (IOException ex) {
            System.out.println("Error al enviar el mensaje " + ex);
        }
    }
    
    public void read() {
        try {
            Resources.Message m = (Resources.Message) this.input.readObject();
            System.out.println("TAMBIEN LEEMOS");
            
        } catch(Exception e) {
            System.out.println(e);
        }
    }
}
