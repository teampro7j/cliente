/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;
import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 *
 * @author root
 */
public class bioGUI extends JFrame implements ActionListener{
    /***
     * Esta es la pantalla en donde se mostraran los contactos,
     * se muestra su nombre de usuario seguido de un boton para comenzar el chat,
     * y cuenta con un boton de refresh para buscar a nuevos usuarios.
     */
    private Socket socket;
    private ObjectInputStream input;
    private ObjectOutputStream output;
    private int ypos;
    private String myName;
    private Map<String, JButton> buttons;
    private Map<String, gui.messageGUI> chats;
    
    public void actionPerformed(ActionEvent e) {
        System.out.println("clicked");
        try {
            String name = ((String) ((JButton)e.getSource()).getClientProperty("name"));
            gui.messageGUI chat = new gui.messageGUI(this.socket,this.input,this.output, name, this.myName);
            chat.setVisible(true);
            Thread t = new Thread(chat);
            chats.put(name, chat);
            t.start();
            System.out.println("Clicked " + name);
        }catch(Exception ex ){
            System.out.println("ERR " + ex);
        }
    }
    
    /***
     * Inicializacion de la interfaz
     * @param socket el socket de la conexion
     * @param input el objeto para recibir informacion
     * @param output el objeto para enviar informacion
     * @param myName el nombre de usuario
     */
    public bioGUI(Socket socket, ObjectInputStream input, ObjectOutputStream output, String myName) {
        this.socket = socket;
        this.input = input;
        this.output = output;
        this.ypos = 50;
        this.myName = myName;
        this.buttons = new HashMap<String, JButton>();
        this.chats = new HashMap<String, gui.messageGUI>();
        initComponents();
        setVisible(true);
        //read();
        
    }
    public void read() {
        /*while(true) {
             try{
                Resources.Message messageObject = (Resources.Message)input.readObject();
                System.out.println("OBJETO RECIBIDO !!");
                
             } catch(Exception e) {
                 
             }
         }*/
    }
    
    public void initComponents() {
        
        
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        setSize(800,800);
        setTitle("Messenger - " + this.myName);
        JButton refresh = new JButton("Refresh");
        refresh.setBounds(10,10,40,40);
        add(refresh);
        refresh.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               
               try {
                System.out.println(input + " Y " + output);
                JSONObject json = new JSONObject();
                json.put("request","users");
                json.put("data",new JSONObject());
                ArrayList<String> users = getUsers(json.toString());
                addButtons(users);
                //ypos = 10;
                System.out.println("get more end");

            } catch(Exception ex) {
                System.out.println("ERROR AL OBTENERO LOS USUARIOS "+ ex);
            } 
            }
        });
        this.addWindowListener(new WindowListener() {
            @Override
            public void windowClosing(WindowEvent e) {
                closeCon();
            }

            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });
        
    }
    
    public void closeCon() {
        System.out.println("Bye");
        JSONObject json = new JSONObject();
        try {
            json.put("request", "logout");
            JSONObject data = new JSONObject();
            data.put("username",this.myName);
            json.put("data", data);
            this.output.writeObject(new Resources.Message(json.toString()));
        } catch(Exception e) {
            System.out.println("Error al cerrar sesion " + e);
        }
    }
    
    /***
     * Añade dinamicamente botones para empezar una conversacion con los usuarios,
     * recibe una lista de usuarios que son los que se agregaran.
     * @param users lista de usuarios a agregar
     */
    public void addButtons(ArrayList<String> users) {
        for(int i = 0; i < users.size(); i++) {

            if(!buttons.containsKey(users.get(i))) {
                JLabel name = new JLabel(users.get(i));

                JButton btn = new JButton("Chatear");

                buttons.put(users.get(i), btn);
                name.setBounds(10, ypos, 200, 30);
                add(name);
                
                btn.setBounds(300, ypos, 400, 30);
                btn.putClientProperty("name", users.get(i));
                btn.addActionListener(this);
                add(btn);
                repaint();
                ypos+=40;
            }             
        }
    }
    
    /***
     * Obtiene a todos los usuarios conectados en el servidor
     * @param response la peticion que se esta haciendo en formato json
     * @return Una lista con el nombre de usuario de todos los usuarios que iniciaron sesion al servidor.
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws JSONException 
     */
    public ArrayList<String> getUsers(String response) throws IOException, ClassNotFoundException, JSONException {
        
        ArrayList<String> result = null;
        
        output.writeObject(new Resources.Message(response));
        System.out.println(input);
        Resources.Message res = (Resources.Message) input.readObject();
        
        JSONObject jsonResult = new JSONObject(res.getData());
        System.out.println("JSON: " + jsonResult);
        JSONArray jsonArr = jsonResult.getJSONArray("data");
        if(jsonResult.get("status").equals("ok")) {
            result = new ArrayList<String>();
            //JSONObject data = jsonResult.getJSONObject("data");
            for(int i = 0; i < jsonArr.length() ; i++) {
                result.add((String) jsonArr.get(i));
            }
        }
        System.out.println("GET USERS RESULT: " + result);
        //input.close();
        return result;
    }
    
}
