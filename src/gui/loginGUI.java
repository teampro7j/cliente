/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import javax.swing.JOptionPane;
import org.json.*;

/**
 *
 * @author root
 */
public class loginGUI extends javax.swing.JFrame {

    /**
     * Creates new form loginGUI, donde se intentara loguear, si lo consigue abrira sus contactos,
     * si no se abrira el formulario para registrarse
     */
    private Socket socket;
    private ObjectOutputStream output = null;
    private ObjectInputStream input = null;
    
    
    public loginGUI(Socket socket, ObjectInputStream input, ObjectOutputStream output) {
        this(socket);
        this.socket = socket;
        //this.input = input;
        //this.output = output;
        System.out.println("Not enter");
    }
    
    public loginGUI(Socket socket) {
        
        initComponents();
        this.socket = socket;
        if(this.input == null && this.output == null) {
            try{
                this.output = new ObjectOutputStream(this.socket.getOutputStream());
                this.input = new ObjectInputStream(this.socket.getInputStream());
            } catch(Exception e) {
                System.out.println("Creacion erronea " + e);
            } 
        }
        System.out.println(this.input + " y " + this.output);
        this.setVisible(true);
        jButton2.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    JSONObject response = new JSONObject();
                    response.put("request", "login");
                    JSONObject data = new JSONObject();
                    data.put("username", jTextField3.getText());
                    data.put("password", jTextField4.getText());
                    response.put("data",data);
                    String []result = attemptLogin(response.toString());
                    if(result[0].equals("ok")) {
                        System.out.println("INICIANDO SESION");
                        
                        gui.bioGUI bio = new gui.bioGUI(socket,input,output, result[1]);
                        //bio.setVisible(true);
                        setVisible(false);
                        dispose();
                    }else {
                        JOptionPane.showMessageDialog(null, "Usuario no encontrado");
                        gui.registerGUI register = new gui.registerGUI(socket,input,output);
                        register.setVisible(true);
                        setVisible(false);
                        dispose();
                    }
                    System.out.println(result);
                    
                } catch(Exception ex) {
                    System.out.println("Error " + ex);
                }
            }
        });
        
    }
    /***
     * Esta funcion intenta loguear al usuario en la aplicacion, en caso de que sea exitoso se abriran
     * sus contactos, de caso contrario se tendra que registrar
     * @param response el json de la peticion
     * @return Un arreglo de strings, el primero siendo la respuesta del servidor y el segundo solo existe
     * cuando se loguea, devolviendo su nombre de usuario
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws JSONException 
     */
    public String[] attemptLogin(String response) throws IOException, ClassNotFoundException, JSONException {
        String []result = new String[2];
        System.out.println("IN LOGIN ATTEMPT login");
        System.out.println(this.output + " y " + this.input);
        this.output.writeObject(new Resources.Message(response));
        Resources.Message res = (Resources.Message) this.input.readObject();
        
        JSONObject jsonResult = new JSONObject(res.getData());
        result[0] = (String) jsonResult.get("response");
        result[1] = (String) jsonResult.get("data");
        //input.close();
        return result;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel3.setFont(new java.awt.Font("DejaVu Sans Light", 1, 24)); // NOI18N
        jLabel3.setText("INICIA SESIÓN");

        jLabel5.setText("Usuario:");

        jLabel6.setText("Contraseña.");
        jLabel6.setToolTipText("");

        jButton2.setText("INGRESAR");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6))
                        .addGap(77, 77, 77)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField4)
                            .addComponent(jTextField3))))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(104, 104, 104)
                .addComponent(jLabel3)
                .addContainerGap(113, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(jLabel3)
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(45, 45, 45)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 49, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addGap(44, 44, 44))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    // End of variables declaration//GEN-END:variables
}
